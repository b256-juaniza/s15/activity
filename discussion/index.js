console.log("Hello World");

// Message Alert

function msg() {

	if (input.includes(greet)) {
		alert("Hello User");
	}
}

let productName = "Desktop Computer"; 
console.log(productName);

let productPrice = 19900;
console.log(productPrice);

// Const are values/information that is constant and cannot be changed.
// This is the best way to prevent application from suddenly breaking or performing in ways that are not intended. 
const interest = 3.530;
console.log(interest);

//Reassigning Variable values
// changing the initial or previous value into another new value

/*
	Syntax:
		variableName = new_value;
*/
productName = 'Laptop';
console.log(productName);

/*let friend = 'Kate'; 
friend = 'Jane';

let friends = 'Kate';
let friends = 'Jane';
console.log()*/

let supplier;

supplier = 'John Smith Tradings';
console.log(supplier);

supplier = 'Jane Smith Tradings';
console.log(supplier);


/*const pi = 3.14;
pi = 3.14;
console.log(pi);*/

/*var vs let/const*/

a = 5;
console.log(a);
var a;

/*
b = 5;
console.log(b);
let b;
*/

let outerVariable = 'hello';

{
	let innerVariable = 'its me again';
}

console.log(outerVariable);
// console.log(innerVariable);

const outerVariable2 = 'hi';

{
	const innerVariable2 = "i'm back";
}

console.log(outerVariable2);
// console.log(innerVariable2);

let word1, word2, word3, word4;

word1 = 'My';
word2 = 'Name';
word3 = 'is';
word4 = 'JayzEe';

console.log(word1,word2,word3,word4);

let country = 'Philippines';
let province = 'Metro Manila';

let address = province + ',' + country;
console.log(address);

/*const anime = ['one piece,','one punch man','Attack on Titan']
anime = ['kimetsu no yaiba'];

console.log(anime);

const anime = ['one piece,','one punch man','Attack on Titan']
*/

const anime = ['one piece,','one punch man','Attack on Titan']

console.log(anime);
anime[0] = 'kimetsu no yaiba';

console.log(anime);

// null 

// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
	let spouse = null;
// Using null compared to a 0 value and an empty string is much better for readability purposes

// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
	let myNumber = 0;
	let myString = '';

	// Undefined
	// Represents the state of a variable that has been declared but without an assigned value
	let fullName;
	console.log(fullName);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing

	let varA = null;
	console.log(varA);

// For undefined, this is normally caused by developers creating variables that have no value/data associated with them
// This is when the value of a variable is still unknown

	let varB;
	console.log(varB);

